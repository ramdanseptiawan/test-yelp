package com.ramdan.testyelpapp.main

import android.app.Application
import android.view.View
import android.widget.RadioGroup
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.ramdan.testyelpapp.network.models.YelpSearch

class YelpViewModel(application: Application) : AndroidViewModel(application) {

    // Network Calls Repository
    private var repository = YelpRepository(application)
    // LiveData to store the results
    private var yelpSearchLiveData = repository.getLiveData()

    /**
     * @return LiveData of YelpSearch
     */
    fun getYelpSearchLiveData(): LiveData<YelpSearch> {
        return yelpSearchLiveData
    }

    /**
     * @param searchTerm the search term to fetch
     */
    fun getYelpSearch(searchTerm: String, sortBy: String) {
        repository.getYelpSearch(searchTerm,sortBy)
    }

    fun getYelpSearchAdress(location: String, sortBy: String) {
        repository.getYelpSearchLocation(location,sortBy)
    }

    fun getYelpSearchCategories(categories: String, sortBy: String) {
        repository.getYelpSearchCategories(categories,sortBy)
    }


}