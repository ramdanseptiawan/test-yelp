package com.ramdan.testyelpapp.network

import com.ramdan.testyelpapp.network.models.YelpSearch
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface RetrofitService  {

    @GET("/v3/businesses/search")
    fun getYelpSearch(
        @Header("Authorization") api: String,
        @Query("term") term: String,
        @Query("sort_by") sort_by: String,
        @Query("latitude") latitude: Double,
        @Query("longitude") longitude: Double
    ): Call<YelpSearch>
    @GET("/v3/businesses/search")
    fun getYelpSearchCategories(
        @Header("Authorization") api: String,
        @Query("categories") categories: String,
        @Query("sort_by") sort_by: String,
        @Query("latitude") latitude: Double,
        @Query("longitude") longitude: Double
    ): Call<YelpSearch>
    @GET("/v3/businesses/search")
    fun getYelpSearchLocation(
        @Header("Authorization") api: String,
        @Query("location") location: String,
        @Query("sort_by") sort_by: String,
    ): Call<YelpSearch>
}